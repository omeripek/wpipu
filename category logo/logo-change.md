# Kategoriye göre logo değiştirme
## İstediğiniz kategoriye özel logo

Wordpress kullanırken bazen özelleştirmeye ihtiyaç duyarız, mesela seçtiğiniz kategori sitenizin içeriğinden farklı göstermek istiyorsunuz ve bunu ziyaretçinize göstermek için o kategoriye özel logo belirleyebilirsiniz.

## Kategori id'sini öğrenerek başlayalım

Kategori id'si için yazıların olduğu kısımda menüde kategorilere geliyoruz, üzerine fare ile geldiğimizde bilgi alt çubuğunda veya link tıkladığımızda adres çubuğunda linki göreceksiniz. Orada id şu sekilde yazmakta:

![Kategori id öğrenme](category_id.png)

# Kategori id öğrendikten sonra logomuzu değiştirmek için birkaç adımdan biri olan aşağıdaki yöntemi uygulayalım.

Wordpress yazımızın hangi kategoride olduğunu, kategori içerisinde miyiz diye kontrol ettiğimiz fonksiyonlarından yararlanalım. (Kategori ID veya Slug değerlerini veya bir dizi girebiliriz.)

in_category // Bool
is_category

Temamızın logo yapısına göre `header.php` sayfamızda mantıksal kontroller ile istediğimiz sonuca ulaşabilir.

## Kategoriye göre logo düzenleme

Kodları kendinize göre düzenleyerek kolaylıkla yapabilirsiniz.

Mesela kategor id 13 olarak gördük ve o kategoride ve o kategoriye ait yazılarda farklı bir logo gösterelim.

```php
	<?php if (in_category( 13 ) || is_category( 13 )) { ?>
		<a href="<?php echo esc_url(home_url( '/' )); ?>">
			<img  src="LOGO LİNKİNİZ" alt="LİNK BAŞLIĞI / BLOG BAŞLIĞI"/> // LOGO HANGİ DİZİNDE İSE "LOGO LİNKİNİZ" KISMINI KENDİNİZE GÖRE DÜZENLEYİN
		</a>
	<?php }else{?>
		// ANA TEMANIZDA BULUNAN VARSAYILAN LOGO 
                <a href="<?php echo esc_url(home_url( '/' )); ?>"> 
			<img src="LOGO LİNKİNİZ" alt="LİNK BAŞLIĞI / BLOG BAŞLIĞI" />
		</a>
	<?php } ?>
```

Ayrıca bu yöntem dışında array vb birçok çeşit vardır. Sırasıyla eklemeye çalışacağım.
